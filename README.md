<h1 align="center" style="font-size : 60px;">Facebik</h1>

## 1 - Présentation

Forum ayant comme sujet principal le numérique. Les utilisateurs pourront se connecter, créer un article, lire un article, modifier un article, supprimer un article, commenter un article, lire un commentaire, modifier un commentaire et supprimer un commentaire.
<br /><br />

## 2 - Cas d'utilisation

### A - Utilisateur

Un utilisateur dispose d’un ID, d’un pseudonyme, d’un email et d’un mot de passe.
<br /><br />

### B - Catégorie

Une catégorie dispose d’un ID et d’un nom
<br /><br />

### C - Articles

Un article dispose d’un id, un titre, d’une description et d’une image (base64 encode)
<br /><br />

### D - Commentaires/Réponses

Un commentaire dispose d’un id, d’un id_article et d’une description
Un article dispose d’un id, un titre, d’une description et d’une image (base64 encode)
<br /><br />

## 3 - Diagramme de classe

<p align="center"><img src="./public/images/diagramme.png"></p>


## 4 - Les tests

### A - Installation et documentation 

Les test de TDD sont fait en PHPUnit, il faut donc installer 

> composer global require phpunit/phpunit

La documentation est sur ce [lien](https://www.kaherecode.com/tutorial/comment-demarrer-le-test-driven-development-avec-laravel)

### B - Lancement des test de features

Pour lancer le test de création d'un article par un utilisateur : 

> phpunit --filter users_can_create_new_article

Pour lancer le test de création d'une categorie : 

> phpunit --filter create_new_categorie

Pour lancer tout les test aussi bien unitaire et features faut lancer :

> phpunit

